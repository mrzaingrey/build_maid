Thanks for taking the time to open an issue for mAid.

Please fill in the following:



### Version

Simply double-click the *Welcome* icon on the desktop or open a terminal and paste the output of: `cat /etc/maid-release` 

Version: TYPE-YOURS



### Description / How to re-produce

describe your bug report / feature request as detailed as possible. Use gists.github.com or drop files to the issue for providing logfiles and such!



### Screenshot(s) / Photo(s)

a great help is to attach a screenshot/photo of the issue/error! This helps in 8 of 10 times to understand an issue faster.



### Links

any links like XDA posts or new tools you want to be added

Examples:

- External reports
- Possible solution or workaround (if you know any)
- Download(s)


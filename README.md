## Build environment/scripts for mAid - [m]anage [A]ndro[id]

The project was named "FWUL (Forget Windows Use Linux)" up to v3.x and has been renamed to "mAid ( [m]anage [A]ndro[id] )" with the release of v4.0.

Rebranding a big project like that is a challenge so you will still find the old "FWUL" brand in several places. That will change but that takes time.

Official XDA thread: https://bit.do/MAIDatXDA

Website: https://maid.binbash.rocks

## Setup & Prepare (Arch Linux)

No other distribution then Arch Linux is supported (for both: build system and release).

1. `pacman -S archiso jq pacman-contrib`
1. `git clone https://code.binbash.rocks:8443/mAid/build.git ~/build_mAid`


## Usage / Build

### 64 bit (only available arch)

This is the only supported architecture for mAid! 

1. `cd ~/build_mAid`
1. `sudo ./build_x64.sh -A x86_64 -F`

Use `./build_x64.sh --help` to find all possible options like working directory etc.


## Rebuild / Update ISO

1. `cd ~/build_mAid`
1. Add the option "-C"  or "-F" option to the instruction in "Usage / Build"

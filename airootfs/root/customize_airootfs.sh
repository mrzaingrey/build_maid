#!/bin/bash
#######################################################################################################

set -e -u

# debug means e.g. SSH server will be added and enabled
# 1 means debug on, 0 off.
DEBUG=0

# when set to 1 it will rebuild packages which might take hours to complete
# e.g. qtwebkit
LONGPKGREBUILD=0

ntpdate de.pool.ntp.org 

locale-gen

ln -sf /usr/share/zoneinfo/UTC /etc/localtime

usermod -s /bin/bash root
cp -aT /etc/skel/ /root/
chmod 700 /root

sed -i "s/#Server/Server/g" /etc/pacman.d/mirrorlist
sed -i 's/#\(Storage=\)auto/\1volatile/' /etc/systemd/journald.conf

sed -i 's/#\(HandleSuspendKey=\)suspend/\1ignore/' /etc/systemd/logind.conf
sed -i 's/#\(HandleHibernateKey=\)hibernate/\1ignore/' /etc/systemd/logind.conf
sed -i 's/#\(HandleLidSwitch=\)suspend/\1ignore/' /etc/systemd/logind.conf

# some vars
TMPSUDOERS=/etc/sudoers.d/build
RSUDOERS=/etc/sudoers.d/mAid
LOGINUSR=android
LOGINPW=linux
RPW=$LOGINPW
TINYCLONE="git clone --depth 1 --no-single-branch"

# set fake release info (get overwritten later)
echo "maidversion=0.0" > /etc/maid-release
echo "maidbuild=0" >> /etc/maid-release
echo "patchlevel=0" >> /etc/maid-release
ln -sf /etc/maid-release /etc/fwul-release

echo "iso version: $iso_version"
echo "iso label: $iso_label"

# add live user but ensure this happens when not there already
echo -e "\nuser setup:"
! id $LOGINUSR && useradd -m -p "" -g users -G "adm,audio,floppy,log,network,rfkill,scanner,storage,optical,power,wheel" -s /bin/bash $LOGINUSR
id $LOGINUSR
passwd $LOGINUSR <<EOSETPW
$LOGINPW
$LOGINPW
EOSETPW

###################################################################
# prepare user home !!! No changes above!
#######################
cp -avT /etc/fwul/ /home/$LOGINUSR/
[ ! -d /home/$LOGINUSR/Desktop ] && mkdir /home/$LOGINUSR/Desktop
chmod 700 /home/$LOGINUSR
chown -R $LOGINUSR /home/$LOGINUSR 

# current java version provided with FWUL (to save disk space compressed and not installed)
CURJAVA="$(ls /home/$LOGINUSR/.fwul/jre*${arch}.pkg.tar.*)"
INSTJAVA="sudo pacman -U --noconfirm $CURJAVA"

# prepare hosts
[ ! -f /etc/hosts.orig ] && cp /etc/hosts /etc/hosts.orig
echo "10.0.228.17     leech.binbash.rocks" >> /etc/hosts

# add user to required groups
usermod -a -G vboxsf $LOGINUSR 

# session hacks
sed -i "s#REPLACEVBOXHOME#/home/$LOGINUSR/Desktop#g" /etc/systemd/scripts/fwul-session.sh
sed -i "s#REPLACEHOME#/home/$LOGINUSR#g" /etc/profile.d/fwul-session.sh
sed -i "s#REPLACEHOME#/home/$LOGINUSR#g" /home/$LOGINUSR/.xprofile
sed -i "s#REPLACEHOME#/home/$LOGINUSR#g" /home/$LOGINUSR/.fwul/*.sh
sed -i "s#REPLACEHOME#/home/$LOGINUSR#g" /usr/share/polkit-1/actions/*.policy
sed -i "s#REPLACEHOME#/home/$LOGINUSR#g" /etc/dconf/db/database.d/*

# temp perms for archiso
[ -f $RSUDOERS ]&& rm -vf $RSUDOERS      # ensures an update build will not fail
cat > $TMPSUDOERS <<EOSUDOERS
ALL     ALL=(ALL) NOPASSWD: ALL
EOSUDOERS

# add the pacman ignore list
# https://code.binbash.it:8443/FWUL/build_fwul/issues/63
sed -i '/\[options\]/a Include=/etc/pacman.ignore' /etc/pacman.conf
cat > /etc/pacman.ignore<<EOPIGN
# PARTIAL UPGRADES ARE NOT SUPPORTED! USE THIS WITH CARE!
# Check this first:
# https://wiki.archlinux.org/index.php/System_maintenance#Partial_upgrades_are_unsupported

# The option IgnorePkg EXTENDS the pacman ignore list.
# Format: ONE package for every ignore line! Do *not* use space delimitated syntax here!

# uefi and syslinux
IgnorePkg = efibootmgr
IgnorePkg = efitools
IgnorePkg = efivar
IgnorePkg = refind-efi
IgnorePkg = syslinux

# kernel, kernel headers and ramdisk tools
IgnorePkg = mkinitcpio-busybox
IgnorePkg = mkinitcpio
IgnorePkg = linux*

# virtualbox module
IgnorePkg = virtualbox-guest-utils

# extreme CPU intensive and usually not needed to upgrade
IgnorePkg = qtwebkit
EOPIGN

if [ $arch == "x86_64" ];then
    # init pacman + multilib
    # O M G ! This is so crappy bullshit! when build.sh see's 
    # the returncode 1 it just stops?!! so I use that funny workaround..
    RET=$(egrep -q '^\[multilib' /etc/pacman.conf||echo missing)
    if [ "$RET" == "missing" ];then
        echo "adding multilib to conf"
        cat >>/etc/pacman.conf<<EOPACMAN
[multilib]
SigLevel = PackageRequired
Include = /etc/pacman.d/mirrorlist
EOPACMAN
    else
        echo skipping multilib because it is configured already
    fi
else
    echo "SKIPPING multilib because of $arch"
fi

# disable free disk check
sed -i 's/^CheckSpace/#CheckSpace/' /etc/pacman.conf

# initialize the needed keyrings (again)
pacman -S --noconfirm gnupg archlinux-keyring manjaro-keyring
haveged -w 1024
pacman-key --init
pacman-key --populate archlinux manjaro
pacman-key --refresh-keys
su -c - $LOGINUSR "trizen -Syyu --noconfirm" || echo "update ended with $?" 

#
# preparing system finished - no regular install stuff above!
#
####################################################################

# install LivePatcher
echo "\nINSTALL LIVE PATCHER:\n"
[ -d /tmp/patcher ] && rm -rf /tmp/patcher
$TINYCLONE https://code.binbash.rocks:8443/mAid/livepatcher-pkg.git /tmp/patcher \
    && cd /tmp/patcher \
    && chown $LOGINUSR /tmp/patcher \
    && su -c - $LOGINUSR "makepkg" \
    && pacman --noconfirm -U livepatcher-*.pkg.tar.*
rm -rf /tmp/patcher
cd /

echo "CURRENT MOUNTS"
df -h

if [ "$iso_label" == "mAid_nightly" ];then
    buildpkgver="nightly"
else
    buildpkgver="v${iso_version}"
fi

F_CLEANPKG(){
    [ -d /tmp/pkgbuild ] && rm -rf /tmp/pkgbuild
    find /var/cache/pacman/pkg/ /var/tmp /tmp/trizen-${LOGINUSR}/ -type f -name *.pkg.tar.* -delete
    mkdir /tmp/pkgbuild
}

#########################
# FWUL pre-pkgs - start -

# create a FWUL pkg 
echo -e "\nMAKEPKG for teamviewer:\n"
RMN=1
pacman -Q teamviewer || RMN=0
if [ $RMN == "0" ];then
    F_CLEANPKG
    su -c - $LOGINUSR "trizen --clone-dir=/var/tmp -S --noconfirm teamviewer"
    find /var/cache/pacman/pkg/ /var/tmp /tmp/trizen-android/ -name *.pkg.tar.* -exec mv -v {} /tmp/pkgbuild/ \;
    tar -cvf /.repo/teamviewer_maid-${buildpkgver}.tar -C /tmp/pkgbuild/ .
    echo -e "\nremove pkg and its deps (HINT THIS COULD REMOVE MORE THEN GOOD):\n"
fi
TVVERS=$(pacman -Q teamviewer | cut -d " " -f2)
echo -e "\nremove pkg and its deps (HINT THIS COULD REMOVE MORE THEN GOOD):\n"
pacman -Q teamviewer && pacman -Rns teamviewer --noconfirm

# create a FWUL pkg 
echo -e "\nMAKEPKG for sony flashtools:\n"
RMN=1
pacman -Q xperia-flashtool || RMN=0
if [ $RMN == "0" ];then
    F_CLEANPKG
    pacman -Q libselinux || su -c - $LOGINUSR "trizen -S --noconfirm libselinux"
    su -c - $LOGINUSR "trizen --clone-dir=/var/tmp -S --noconfirm xperia-flashtool"
    find /var/cache/pacman/pkg/ /var/tmp /tmp/trizen-android/ -name *.pkg.tar.* -exec mv -v {} /tmp/pkgbuild/ \;
    tar -cvf /.repo/xperia-flashtool_maid-${buildpkgver}.tar -C /tmp/pkgbuild/ .
    echo -e "\nremove pkg and its deps (HINT THIS COULD REMOVE MORE THEN GOOD):\n"
fi
SONYVERS=$(pacman -Q xperia-flashtool | cut -d " " -f2)
echo -e "\nremove pkg and its deps (HINT THIS COULD REMOVE MORE THEN GOOD):\n"
pacman -Q xperia-flashtool && pacman -Rns --noconfirm xperia-flashtool 
pacman -Q libselinux && pacman -Rns --noconfirm libselinux

# create a FWUL pkg 
echo -e "\nMAKEPKG for sp flashtools:\n"
RMN=1
pacman -Q spflashtool-bin || RMN=0
if [ $RMN == "0" ];then
    F_CLEANPKG
    [ $LONGPKGREBUILD -eq 1 ] && su -c - $LOGINUSR "trizen --clone-dir=/var/tmp -S --noconfirm qtwebkit"
    [ $LONGPKGREBUILD -eq 0 ] && cp -v /.repo/qtwebkit*.pkg.tar.* /tmp/pkgbuild/ && pacman --noconfirm -U /tmp/pkgbuild/qtwebkit*.pkg.tar.*
    su -c - $LOGINUSR "trizen --clone-dir=/var/tmp -S --noconfirm spflashtool-bin"
    find /var/cache/pacman/pkg/ /var/tmp /tmp/trizen-android/ -name *.pkg.tar.* -exec mv -v {} /tmp/pkgbuild/ \;
    tar -cvf /.repo/spflashtool-bin_maid-${buildpkgver}.tar -C /tmp/pkgbuild/ .
    echo -e "\nremove pkg and its deps (HINT THIS COULD REMOVE MORE THEN GOOD):\n"
fi
SPFTVERS=$(pacman -Q spflashtool-bin | cut -d " " -f2)
echo -e "\nremove pkg and its deps (HINT THIS COULD REMOVE MORE THEN GOOD):\n"
pacman -Q spflashtool-bin && pacman -Rns --noconfirm spflashtool-bin
pacman -Q qtwebkit && pacman -Rns --noconfirm qtwebkit

# create a FWUL pkg 
echo -e "\nMAKEPKG for chromium:\n"
RMN=1
pacman -Q chromium || RMN=0
if [ $RMN == "0" ];then
    F_CLEANPKG
    pacman -S --noconfirm chromium
    find /var/cache/pacman/pkg/ /var/tmp /tmp/trizen-android/ -name *.pkg.tar.* -exec mv -v {} /tmp/pkgbuild/ \;
    tar -cvf /.repo/chromium_maid-${buildpkgver}.tar -C /tmp/pkgbuild/ .
    echo -e "\nremove pkg and its deps (HINT THIS COULD REMOVE MORE THEN GOOD):\n"
fi
CHRVERS=$(pacman -Q chromium | cut -d " " -f2)
echo -e "\nremove pkg and its deps (HINT THIS COULD REMOVE MORE THEN GOOD):\n"
pacman -Q chromium && pacman -Rns --noconfirm chromium

# create a FWUL pkg 
echo -e "\nMAKEPKG for firefox:\n"
RMN=1
pacman -Q firefox || RMN=0
if [ $RMN == "0" ];then
    F_CLEANPKG
    pacman -S --noconfirm firefox
    find /var/cache/pacman/pkg/ /var/tmp /tmp/trizen-android/ -name *.pkg.tar.* -exec mv -v {} /tmp/pkgbuild/ \;
    tar -cvf /home/$LOGINUSR/.fwul/ff.tar -C /tmp/pkgbuild/ .
fi
FFVERS=$(pacman -Q firefox | cut -d " " -f2)
echo -e "\nremove pkg and its deps (HINT THIS COULD REMOVE MORE THEN GOOD):\n"
pacman -Q firefox && pacman -Rns --noconfirm firefox


# create a FWUL pkg 
echo -e "\nMAKEPKG for mAid installer:\n"
RMN=1
PKGS="calamares rsync mkinitcpio squashfs-tools gnu-free-fonts os-prober"
F_CLEANPKG
for PKG in $PKGS;do
    pacman -Sw --noconfirm $PKG
done
find /var/cache/pacman/pkg/ /var/tmp /tmp/trizen-android/ -name *.pkg.tar.* -exec mv -v {} /tmp/pkgbuild/ \;
tar -cvf /.repo/maidsetup_maid-${buildpkgver}.tar -C /tmp/pkgbuild/ .

# FWUL pre-pkgs - end -
#########################

# install yad
echo -e "\nyad (latest STABLE):"
trizen -Q yad || echo "yad will be installed now..."
#su -c - $LOGINUSR "trizen -S --noconfirm yad"
# WORKAROUND DUE TO HEAVY CHANGES IN YAD - SWITCHED TO mAid own repo
OLPW=$(pwd)
[ -d /tmp/maid-yad ]&& rm -rf /tmp/maid-yad
$TINYCLONE https://code.binbash.rocks:8443/mAid/maid-yad.git /tmp/maid-yad
chown -R $LOGINUSR /tmp/maid-yad
cd /tmp/maid-yad
su -c - $LOGINUSR "makepkg --noconfirm -is"
cd $OLPW
rm -rf /tmp/maid-yad

# install python zstandard for newer extracting lg kdz
PZWORKAROUND=n
trizen -Q python-zstandard || su -c - $LOGINUSR "trizen -S --noconfirm python-zstandard" || PZWORKAROUND=y
# as python-zstandard is in the transition to become a binary package (yeeeha) it is in testing atm and so need this workaround
# because it has been removed from AUR already
if [ "$PZWORKAROUND" == "y" ];then
    wget -O /tmp/pz.pkg.tar.zst http://manjaro.melbourneitmirror.net/testing/community/x86_64/python-zstandard-0.12.0-1-x86_64.pkg.tar.zst
    pacman --noconfirm -U /tmp/pz.pkg.tar.zst
    rm /tmp/pz.pkg.tar.zst
fi 

# FIXME: (should be ok now since the above implementation)
# workaround until fixed upstream (only activated when AUR package isn't fixed)
# https://aur.archlinux.org/packages/python-zstandard/#comment-674955
if [ ! -d /usr/lib/python3.8/site-packages/zstandard ];then
    $TINYCLONE https://aur.archlinux.org/python-zstandard.git /tmp/zstd 
    chown -R $LOGINUSR /tmp/zstd
    cd /tmp/zstd
    su -c - $LOGINUSR "makepkg"
    ls -la src/
    cp -av src/python-zstandard-*/zstandard /usr/lib/python3.8/site-packages/
    echo "WORKAROUND ADDED FOR PYTHON-ZSTANDARD!"
    rm -rf /tmp/zstd
fi

# GUI based package manager
cp /usr/share/applications/pamac-manager.desktop /home/$LOGINUSR/Desktop/
sed -i 's/Icon=.*/Icon=gnome-software/g' /home/$LOGINUSR/Desktop/pamac-manager.desktop

# disable tray to avoid bothering users for updating
[ -f /etc/xdg/autostart/pamac-tray.desktop ] && rm /etc/xdg/autostart/pamac-tray.desktop

# prepare Samsung tool dir
[ ! -d /home/$LOGINUSR/Desktop/Samsung ] && mkdir /home/$LOGINUSR/Desktop/Samsung

# install udev-rules
[ ! -d /home/$LOGINUSR/.android ] && mkdir /home/$LOGINUSR/.android
[ ! -f /home/$LOGINUSR/.android/adb_usb.ini ] && wget https://github.com/M0Rf30/android-udev-rules/raw/704f83d8528a56c969dab8a9b63bf83279d4c71b/adb_usb.ini -O /home/$LOGINUSR/.android/adb_usb.ini

# always update the udev rules to be top current
wget https://raw.githubusercontent.com/M0Rf30/android-udev-rules/master/51-android.rules -O /etc/udev/rules.d/51-android.rules

# add adb group & perms
groupadd -f adbusers
usermod -a -G adbusers $LOGINUSR

# install & prepare tmate
echo -e "\ntmate:"
trizen -Q tmate || su -c - $LOGINUSR "trizen -S --noconfirm tmate"
# install the ssh keygenerator
[ ! -d /home/$LOGINUSR/.config/autostart ] && mkdir -p /home/$LOGINUSR/.config/autostart && chown -R $LOGINUSR /home/$LOGINUSR/.config/autostart
echo -e '#!/bin/bash\n[ !'" -f /home/$LOGINUSR/.ssh/id_rsa ] && ssh-keygen -a 100 -t ed25519 -f /home/$LOGINUSR/.ssh/id_ed25519 -P ''" > /home/$LOGINUSR/.fwul/sshkeygen.sh
cat > /home/$LOGINUSR/.config/autostart/sshkeygen.desktop<<EOSSHKG
[Desktop Entry]
Version=1.0
Type=Application
Comment=sshkeygen
Terminal=false
Name=sshkeygen
Exec=/home/$LOGINUSR/.fwul/sshkeygen.sh
EOSSHKG
chmod +x /home/$LOGINUSR/.fwul/sshkeygen.sh

# hexchat config: ensure username replacement on every boot (unless persistent install)
echo -e '#!/bin/bash\nif [ !'" -f /home/$LOGINUSR/.config/hexchat/.configured ];then GEN=\$(pwgen -BA01); sed -i s#maid_REPLACE#maid_\${GEN}#g /home/$LOGINUSR/.config/hexchat/hexchat.conf && > /home/$LOGINUSR/.config/hexchat/.configured; fi" > /home/$LOGINUSR/.fwul/hex.sh
cat > /home/$LOGINUSR/.config/autostart/hex.desktop<<EOSSHKG
[Desktop Entry]
Version=1.0
Type=Application
Comment=autostarted on login
Terminal=false
Name=hexchat configuration
Exec=/home/$LOGINUSR/.fwul/hex.sh
EOSSHKG
chmod +x /home/$LOGINUSR/.fwul/hex.sh

cat > /home/$LOGINUSR/Desktop/io.github.Hexchat.desktop <<EODH
[Desktop Entry]
Version=1.0
Type=Application
Comment=IRC client
Terminal=false
Name=IRC (hexchat)
Icon=hexchat
Categories=GTK;Network;IRCClient;
X-GNOME-UsesNotifications=true
Keywords=IM;Chat;
Exec=hexchat --existing %U
EODH
chown $LOGINUSR /home/$LOGINUSR/Desktop/io.github.Hexchat.desktop
chmod +x /home/$LOGINUSR/Desktop/io.github.Hexchat.desktop

# ensure the screenlock background gets set properly
cat > /home/$LOGINUSR/.config/autostart/z_setlockbg.desktop <<EOLBG
[Desktop Entry]
Version=1.0
Type=Application
Comment=autostarted on login
Terminal=false
Name=set background for screenlock
Exec=/home/$LOGINUSR/.xprofile
EOLBG
chmod +x /home/$LOGINUSR/.xprofile

# install & add Heimdall
echo -e "\nheimdall:"
trizen -Q heimdall-git || su -c - $LOGINUSR "trizen -S --noconfirm heimdall-git"
cp /usr/share/applications/heimdall.desktop /home/$LOGINUSR/Desktop/Samsung/
# fix missing heimdall icon
sed -i "s#Icon=.*#Icon=/home/$LOGINUSR/.fwul/heimdall.png#g" /home/$LOGINUSR/Desktop/Samsung/heimdall.desktop

# install testdisk/photorec incl. GUI support
# (https://code.binbash.it:8443/FWUL/build_fwul/issues/66)
echo -e "\nphotorec:"
trizen -Q qt5-tools || pacman --noconfirm -S qt5-tools
#trizen -Q testdisk-wip || su -c - $LOGINUSR "trizen -S --noconfirm testdisk-wip"
# TEMP WORKAROUND UNTIL FIXED UPSTREAM:
# https://aur.archlinux.org/packages/testdisk-wip/#comment-705767
OLPW=$PWD
mkdir /tmp/tdwip
cd /tmp/tdwip
cat > PKGBUILD <<EOPB
# Maintainer: Steven Honeyman <stevenhoneyman at gmail com>

pkgname=testdisk-wip
pkgver=7.2
pkgrel=1
pkgdesc="Checks and undeletes partitions. Includes PhotoRec signature based recovery tool. WIP version"
arch=('i686' 'x86_64')
url="http://www.cgsecurity.org/wiki/TestDisk"
license=('GPL')
depends=('libjpeg')
optdepends=('libewf: support EnCase files'
            'ntfs-3g: support NTFS partitions'
            'progsreiserfs: support ReiserFS partitions'
            'qt5-base: QPhotoRec GUI')
conflicts=('testdisk')
provides=('testdisk')
source=(http://www.cgsecurity.org/testdisk-\$pkgver-WIP.tar.bz2)
#md5sums=('5d239865543019227c4ac5052066da6f')
md5sums=('SKIP')  # they update the source without changing the filename...

prepare() {
  cd "\$srcdir/testdisk-\$pkgver-WIP"

  # Some fixes to make it Qt5 capable
  sed -i '/<QWidget>/d' src/*.h
  sed -i '/<QWidget>/d' src/*.cpp
  sed -i 's/Qt::escape\(.*\)text())/QString\1text()).toHtmlEscaped()/g' src/qphotorec.cpp

  ## You might not need these, but I did for some reason
  test -f src/gnome/help-about.png || cp /usr/share/icons/gnome/48x48/actions/help-about.png src/gnome/
  test -f src/gnome/image-x-generic.png || cp /usr/share/icons/gnome/48x48/mimetypes/image-x-generic.png src/gnome/
}

build() {
  cd "\$srcdir/testdisk-\$pkgver-WIP"
  export QTGUI_LIBS="\$(pkg-config Qt5Widgets --libs)"
  export QTGUI_CFLAGS="-fPIC \$(pkg-config Qt5Widgets --cflags)"

  # add --disable-qt if you do not have qt5-base
  ./configure --prefix=/usr --enable-sudo
  make
}

package() {
  cd "\$srcdir/testdisk-\$pkgver-WIP"
  make DESTDIR="\$pkgdir" install
}

EOPB
chown $LOGINUSR /tmp/tdwip
su -c - $LOGINUSR "makepkg --noconfirm -is"
cd $OLPW

cp -v /usr/share/applications/qphotorec.desktop /home/$LOGINUSR/Desktop/
chmod +x /home/$LOGINUSR/Desktop/qphotorec.desktop 
sed -i "s#Icon=.*#Icon=/home/$LOGINUSR/.fwul/qphotorec.png#g" /home/$LOGINUSR/Desktop/qphotorec.desktop
sed -i 's#^Exec=.*#Exec=sudo /usr/bin/qphotorec %F#g' /home/$LOGINUSR/Desktop/qphotorec.desktop
chown $LOGINUSR /home/$LOGINUSR/Desktop/qphotorec.desktop

# install welcome screen
echo -e "\nwelcome-screen:"
if [ ! -d /home/$LOGINUSR/programs/welcome ];then
    $TINYCLONE https://code.binbash.rocks:8443/mAid/welcome.git /home/$LOGINUSR/programs/welcome
    # install the regular welcome screen
    if [ ! -f /home/$LOGINUSR/.config/autostart/welcome.desktop ];then
        [ ! -d /home/$LOGINUSR/.config/autostart ] && mkdir -p /home/$LOGINUSR/.config/autostart && chown -R $LOGINUSR /home/$LOGINUSR/.config/autostart
        cat > /home/$LOGINUSR/.config/autostart/welcome.desktop<<EOWAS
[Desktop Entry]
Version=1.0
Type=Application
Comment=FWUL Welcome Screen
Terminal=false
Name=Welcome
Exec=/home/$LOGINUSR/programs/welcome/welcome.sh
Icon=/home/$LOGINUSR/programs/welcome/icons/welcome.png
EOWAS
    fi
    # install the force welcome screen (when user manually want to start it)
    if [ ! -f /home/$LOGINUSR/Desktop/welcome.desktop ];then
        cat > /home/$LOGINUSR/Desktop/welcome.desktop <<EOWASF
[Desktop Entry]
Version=1.0
Type=Application
Comment=FWUL Welcome Screen
Terminal=false
Name=Welcome
Exec=/home/$LOGINUSR/programs/welcome/welcome-force.sh
Icon=/home/$LOGINUSR/programs/welcome/icons/welcome.png
EOWASF
    fi
    chmod +x /home/$LOGINUSR/.config/autostart/welcome.desktop /home/$LOGINUSR/Desktop/welcome.desktop
fi

# install workaround script for language bug
# https://code.binbash.it:8443/FWUL/build_fwul/issues/88
[ ! -d /home/$LOGINUSR/.config/autostart ] && mkdir -p /home/$LOGINUSR/.config/autostart && chown -R $LOGINUSR /home/$LOGINUSR/.config/autostart 
cat > /home/$LOGINUSR/.config/autostart/language.desktop<<EOLNGBUG
[Desktop Entry]
Version=1.0
Type=Application
Comment=autostarted on login
Terminal=false
Name=language workaround
Exec=/home/android/.fwul/checklang.sh
EOLNGBUG
chmod +x /home/$LOGINUSR/.fwul/checklang.sh

# install JOdin3
if [ $arch == "x86_64" ];then
    if [ ! -d /home/$LOGINUSR/programs/JOdin ];then
        mkdir /home/$LOGINUSR/programs/JOdin
        cat >/home/$LOGINUSR/programs/JOdin/starter.sh <<EOEXECOD
#!/bin/bash
trizen -Q jre8
if [ \$? -ne 0 ];then 
    $INSTJAVA | stdbuf -i0 -o0 -e0 sed 's/^/# /g' | yad --center --title Install_Java --width=600 --height=400 --progress --enable-log="installing java ..." --auto-close --log-expanded --auto-kill --button=Abort:9 --pulsate
fi
JAVA_HOME=/usr/lib/jvm/java-8-jre /home/$LOGINUSR/programs/JOdin/JOdin3CASUAL
EOEXECOD
        chmod +x /home/$LOGINUSR/programs/JOdin/starter.sh
        wget "http://localhost:8008/misc/JOdin3CASUAL-r1142-dist.tar.gz" -O JOdin.tgz
        tar -xzf JOdin.tgz -C /home/$LOGINUSR/programs/JOdin/ && rm -rf /home/$LOGINUSR/programs/JOdin/runtime JOdin.tgz
        cat >/home/$LOGINUSR/Desktop/Samsung/JOdin.desktop <<EOODIN
[Desktop Entry]
Version=1.0
Type=Application
Comment=Odin for Linux
Terminal=false
Name=JOdin3
Exec=/home/$LOGINUSR/programs/JOdin/starter.sh
Icon=/home/$LOGINUSR/.fwul/odin-logo.jpg
EOODIN
        chmod +x /home/$LOGINUSR/Desktop/Samsung/JOdin.desktop
    fi
else
    echo "SKIPPING JODIN INSTALL: Arch $arch detected!"
fi

# chromium installer
cat >/home/$LOGINUSR/Desktop/install-chromium.desktop <<EOsflashinst
[Desktop Entry]
Version=1.0
Type=Application
Comment=Chromium Browser Installer
Terminal=false
Name=Chromium installer
Exec=/home/$LOGINUSR/.fwul/install_chromium.sh
Icon=anaconda
EOsflashinst
chmod +x /home/$LOGINUSR/Desktop/install-chromium.desktop

cat >/home/$LOGINUSR/.fwul/sonyflash.desktop <<EOsflash
[Desktop Entry]
Version=1.0
Type=Application
Comment=Sony FlashTool
Terminal=false
Name=Sony Flashtool
Exec=xperia-flashtool
Icon=/home/$LOGINUSR/.fwul/flashtool-icon.png
EOsflash

# teamviewer installer
echo -e "\nteamviewer:"
cat >/home/$LOGINUSR/Desktop/install-TV.desktop <<EOODIN
[Desktop Entry]
Version=1.0
Type=Application
Comment=Teamviewer installer
Terminal=false
Name=TeamViewer Installer
Exec=pkexec /home/$LOGINUSR/.fwul/install_tv.sh
Icon=anaconda
EOODIN
chmod +x /home/$LOGINUSR/Desktop/install-TV.desktop

# even when there is an old/outdated version of spflashtool on an old website it is not possible to 
# get it working on 32bit
# http://spflashtool.org/download/SP_Flash_Tool_Linux_32Bit_v5.1520.00.100.zip <--- THIS CONTAINS 64bit BINARIES!!!
if [ $arch == "x86_64" ];then
    # SP Flash Tools installer
    echo -e "\nSP Flash Tools:"
    cat >/home/$LOGINUSR/Desktop/install-spflash.desktop <<EOSPF
[Desktop Entry]
Version=1.0
Type=Application
Comment=SP FlashTools installer
Terminal=false
Name=SP FlashTools Installer
Exec=/home/$LOGINUSR/.fwul/install_spflash.sh
Icon=anaconda
EOSPF
    chmod +x /home/$LOGINUSR/Desktop/install-spflash.desktop
fi

# Sony Flash Tools installer
echo -e "\nSony Flash Tools:"
cat >/home/$LOGINUSR/Desktop/install-sonyflash.desktop <<EOSFT
[Desktop Entry]
Version=1.0
Type=Application
Comment=Sony FlashTools installer
Terminal=false
Name=Sony FlashTools Installer
Exec=/home/$LOGINUSR/.fwul/install_sonyflash.sh
Icon=anaconda
EOSFT
chmod +x /home/$LOGINUSR/Desktop/install-sonyflash.desktop

# prepare LG tools
[ ! -d /home/$LOGINUSR/Desktop/LG ] && mkdir /home/$LOGINUSR/Desktop/LG/

# SALT
echo -e "\nSALT:"
[ ! -d /home/$LOGINUSR/programs/SALT ] && $TINYCLONE https://github.com/steadfasterX/salt.git /home/$LOGINUSR/programs/SALT
[ ! -d /home/$LOGINUSR/programs/lglafng ] && $TINYCLONE https://github.com/steadfasterX/lglaf.git /home/$LOGINUSR/programs/lglafng
[ ! -d /home/$LOGINUSR/programs/kdztools ] && $TINYCLONE https://github.com/steadfasterX/kdztools.git /home/$LOGINUSR/programs/kdztools
[ ! -d /home/$LOGINUSR/programs/sdat2img ] && $TINYCLONE https://github.com/xpirt/sdat2img.git /home/$LOGINUSR/programs/sdat2img
if [ ! -f /home/$LOGINUSR/Desktop/LG/SALT.desktop ];then
    cat > /home/$LOGINUSR/Desktop/LG/SALT.desktop <<EOFDSK
[Desktop Entry]
Version=1.0
Type=Application
Terminal=false
Name=SALT
Icon=/home/$LOGINUSR/programs/SALT/icons/salt_icon.png
Comment=SALT - [S]teadfasterX [A]ll-in-one [L]G [T]ool
Exec=pkexec /home/$LOGINUSR/programs/SALT/salt
EOFDSK
fi
if [ ! -f /home/$LOGINUSR/Desktop/LG/SALT_fb.desktop ];then
    cat > /home/$LOGINUSR/Desktop/LG/SALT_fb.desktop <<EOFDSKFB
[Desktop Entry]
Version=1.0
Type=Application
Terminal=true
Name=SALT (debug)
Icon=/home/$LOGINUSR/programs/SALT/icons/salt_icon.png
Comment=SALT - [S]teadfasterX [A]ll-in-one [L]G [T]ool
Exec=pkexec /home/$LOGINUSR/programs/SALT/salt
EOFDSKFB
fi
chmod +x /home/$LOGINUSR/Desktop/LG/*.desktop

# pure LG LAF
echo -e "\npure lglaf:"
[ ! -d /home/$LOGINUSR/programs/lglaf ] && $TINYCLONE https://github.com/Lekensteyn/lglaf.git /home/$LOGINUSR/programs/lglaf

# LG LAF shortcut with auth
echo -e "\nLG LAF shortcut with auth:"
cat >/home/$LOGINUSR/Desktop/LG/open-lglafshell.desktop <<EOSFT
[Desktop Entry]
Version=1.0
Type=Application
Comment=LG LAF
Terminal=false
Name=LG LAF (PeterWu)
Exec=deepin-terminal --work-directory=/home/$LOGINUSR/programs/lglaf
Icon=terminal
EOSFT
chmod +x /home/$LOGINUSR/Desktop/LG/open-lglafshell.desktop

# LGLAF (steadfasterX) 
echo -e "\nLG LAF NG shortcut:"
cat >/home/$LOGINUSR/Desktop/LG/open-lglafng.desktop <<EOLAFNG
[Desktop Entry]
Version=1.0
Type=Application
Comment=LG LAF with steadfasterX patches
Terminal=false
Name=LG LAF (steadfasterX)
Exec=deepin-terminal --work-directory=/home/$LOGINUSR/programs/lglafng
Icon=terminal
EOLAFNG

# LGLAF (runningnak3d)
[ ! -d /home/$LOGINUSR/programs/lglafsploit ] && $TINYCLONE https://gitlab.com/runningnak3d/lglaf.git /home/$LOGINUSR/programs/lglafsploit
echo -e "\nLG LAF lafsploit shortcut:"
cat >/home/$LOGINUSR/Desktop/LG/open-lglafsploit.desktop <<EOLAFNG
[Desktop Entry]
Version=1.0
Type=Application
Comment=LG LAF with runningnak3d patches
Terminal=false
Name=LG LAF (runningnak3d)
Exec=deepin-terminal --work-directory=/home/$LOGINUSR/programs/lglafsploit
Icon=terminal
EOLAFNG

# tmate
echo -e "\ntmate shortcut:"
cat >/home/$LOGINUSR/Desktop/tmate.desktop <<EOTMATE
[Desktop Entry]
Name=tmate - Simple Remote Control
GenericName=tmate
Comment=Terminal sharing with tmate
Exec=deepin-terminal --window-mode=maximize -e /home/$LOGINUSR/.fwul/tmate.sh
Icon=/home/$LOGINUSR/.fwul/tmate-logo.png
Terminal=false
Type=Application
EOTMATE
chmod +x /home/$LOGINUSR/.fwul/tmate.sh

# firefox
echo -e "\nff shortcut:"
[ ! -d /home/$LOGINUSR/.local/share/applications ] && mkdir -p /home/$LOGINUSR/.local/share/applications && chown $LOGINUSR -R /home/$LOGINUSR/.local/
cat > /home/$LOGINUSR/.local/share/applications/ff.desktop <<EOFF
[Desktop Entry]
Name=mAid Firefox
GenericName=firefox
Comment=Unpacks firefox if needed
Exec=/home/$LOGINUSR/.fwul/unpack_ff.sh
Icon=firefox
Terminal=false
Type=Application
EOFF
chmod +x /home/$LOGINUSR/.fwul/unpack_ff.sh

# install display manager
echo -e "\nDM:"
systemctl enable lightdm

# configure login/display manager
#cp -v /home/$LOGINUSR/.fwul/mdm.conf /etc/mdm/custom.conf
cp -v /home/$LOGINUSR/.fwul/lightdm-gtk-greeter.conf /etc/lightdm/
# copy background + icon for greeter
cp -v /home/$LOGINUSR/.fwul/fwul_login.png /usr/share/pixmaps/greeter_background.png
cp -v  /home/$LOGINUSR/.fwul/greeter_icon.png /usr/share/pixmaps/

# Special things needed for easier DEBUGGING
if [ "$DEBUG" -eq 1 ];then
    pacman -Q openssh || pacman -S --noconfirm openssh
    systemctl enable sshd
    pacman -Q spice-vdagent || pacman -S --noconfirm spice-vdagent
    systemctl enable spice-vdagentd
fi

# add simple ADB 
# (https://forum.xda-developers.com/android/software/revive-simple-adb-tool-t3417155, https://github.com/mhashim6/Simple-ADB)
cat >/home/$LOGINUSR/Desktop/ADB.desktop <<EOSADB
[Desktop Entry]
Version=1.0
Type=Application
Comment=adb and fastboot GUI
Terminal=false
Name=Simple-ADB
Exec=/home/$LOGINUSR/programs/sadb/starter.sh
Icon=/home/$LOGINUSR/programs/sadb/sadb.jpg
EOSADB

cat >/home/$LOGINUSR/programs/sadb/starter.sh <<EOEXECADB
#!/bin/bash
trizen -Q jre8
if [ \$? -ne 0 ];then 
    $INSTJAVA | stdbuf -i0 -o0 -e0 sed 's/^/# /g' | yad --center --title Install_Java --width=600 --height=400 --progress --enable-log="installing java ..." --auto-close --log-expanded --auto-kill --button=Abort:9 --pulsate
fi
java -jar /home/$LOGINUSR/programs/sadb/S-ADB.jar
EOEXECADB
chmod +x /home/$LOGINUSR/programs/sadb/starter.sh

# install android tools (see #8)
pacman -Q abootimg-git || su -c - $LOGINUSR "trizen -S --noconfirm abootimg-git" 
pacman -Q bootimgtool-git || su -c - $LOGINUSR "trizen -S --noconfirm bootimgtool-git" 

# install Xiaomi MiFlash
# https://code.binbash.it:8443/FWUL/build_fwul/issues/62
if [ ! -d /home/$LOGINUSR/programs/MiFlash ];then
    $TINYCLONE https://github.com/limitedeternity/MiFlash-Linux.git /home/$LOGINUSR/programs/MiFlash
    # ensure the stupid setup do not run in FWUL
    > /home/$LOGINUSR/.xiaomi_tool
    # fix help
    ln -s /home/$LOGINUSR/programs/MiFlash/README.md /home/$LOGINUSR/programs/MiFlash/Xiaomi_MiFlash/.bin/xiaomi_tools/README.txt
    sed -i 's/gedit/leafpad/g' /home/$LOGINUSR/programs/MiFlash/Xiaomi_MiFlash/.bin/xiaomi_tools/xiaomi_tools.cfg
    # catch the icon
    wget -O /home/$LOGINUSR/programs/MiFlash/Xiaomi.png https://www.xiaomiflash.com/img/Xiaomi.png
    # make it usable ( - do not use go.sh - )
    cat >/home/$LOGINUSR/programs/MiFlash/starter.sh <<EOEXECXIA
#!/bin/bash
cd /home/$LOGINUSR/programs/MiFlash/Xiaomi_MiFlash
deepin-terminal -x /home/$LOGINUSR/programs/MiFlash/Xiaomi_MiFlash/.bin/xiaomi_tools/xiaomi_tools.cfg -w /home/$LOGINUSR/programs/MiFlash/Xiaomi_MiFlash
EOEXECXIA
    chmod +x /home/$LOGINUSR/programs/MiFlash/starter.sh

    [ ! -d /home/$LOGINUSR/Desktop/Xiaomi/ ] && mkdir /home/$LOGINUSR/Desktop/Xiaomi  && chown $LOGINUSR /home/$LOGINUSR/Desktop/Xiaomi
    cat >/home/$LOGINUSR/Desktop/Xiaomi/miflash.desktop <<EODXIA
[Desktop Entry]
Version=1.0
Type=Application
Comment=
Terminal=false
Name=Xiaomi MiFlash
Exec=/home/$LOGINUSR/programs/MiFlash/starter.sh
Icon=/home/$LOGINUSR/programs/MiFlash/Xiaomi.png
Path=/home/$LOGINUSR/programs/MiFlash/
EODXIA
    # working dir shortcut
    echo -e "\nMiFlash working dir shortcut:"
    ln -s /home/$LOGINUSR/programs/MiFlash/ /home/$LOGINUSR/Desktop/Xiaomi/MiFlash_WorkDir
fi

# install payload extractor
[ ! -d /home/$LOGINUSR/programs/ROME ] && $TINYCLONE https://code.binbash.rocks:8443/mAid/android_rome.git /home/$LOGINUSR/programs/ROME
echo -e "\nROME shortcut:"
cat >/home/$LOGINUSR/Desktop/rome.desktop <<EOXAPE
[Desktop Entry]
Version=1.0
Type=Application
Comment=ROME [ROM] [E]xtractor
Terminal=false
Name=ROME
Exec=/home/$LOGINUSR/programs/ROME/rome
Icon=/home/$LOGINUSR/programs/ROME/icons/rome_icon.png
EOXAPE
chmod +x /home/$LOGINUSR/Desktop/rome.desktop

# add DE effect mgm gui
echo -e "\nDE effects shortcut:"
cat >/home/$LOGINUSR/Desktop/effects.desktop <<EOEFF
[Desktop Entry]
Version=1.0
Type=Application
Comment=Manage Desktop Effects
Terminal=false
Name=Desktop Effects
Exec=/home/$LOGINUSR/.fwul/set_de_effects.sh
Icon=input-gaming
EOEFF
chmod +x /home/$LOGINUSR/Desktop/effects.desktop

# mAid installer
echo -e "\nmAid installer:"
cat > /home/$LOGINUSR/Desktop/install-maid.desktop <<EODESK
[Desktop Entry]
Version=1.0
Type=Application
Comment=Flash mAid to your disk
Terminal=false
Name=Install mAid
Exec=sudo /home/$LOGINUSR/.fwul/install-disk.sh
Icon=/home/$LOGINUSR/.fwul/mAid_installer_icon.png
EODESK
chmod +x /home/$LOGINUSR/Desktop/install-maid.desktop


# make all desktop files usable
chmod +x /home/$LOGINUSR/Desktop/*.desktop /home/$LOGINUSR/Desktop/*/*.desktop
chown -R $LOGINUSR /home/$LOGINUSR/Desktop/

# enable services
systemctl enable pacman-init.service # without dependency to gnupg mount in RAM
systemctl enable choose-mirror.service
systemctl set-default graphical.target
systemctl enable systemd-networkd
systemctl enable NetworkManager
systemctl enable init-mirror
systemctl enable bluetooth
#SEE UPSTREAM SECTION: systemctl enable vboxservice
systemctl enable fwul-session

# disable services
systemctl disable etc-pacman.d-gnupg.mount # fix missing trust db's

# Create a MD5 for a given file or set to 0 if file is missing
F_DOMD5(){
    MFILE="$1"
    [ -z "$MFILE" ]&& echo "MISSING ARG FOR $FUNCNAME!" && exit 3
    if [ -f "$MFILE" ];then
        md5sum $MFILE | cut -d " " -f 1
    else
        echo 0
    fi
}

# wait until a file gets written or modified
F_FILEWAIT(){
    MD5C=$1
    FILE="$2"

    [ -z "$FILE" -o -z "$MD5C" ] && echo "MISSING ARG FOR $FUNCNAME!" && exit 3
    while true; do
        MD5NOW=$(F_DOMD5 "$FILE")
        if [ "$MD5C" != "$MD5NOW" ];then
            break
        else
            echo -e "\t.. waiting that the changes gets written for $FILE..\n\t($MD5C vs $MD5NOW)"
            sleep 1s
        fi
    done
}

# install new crypto lib for SALT/lglaf
pip install cryptography

# python2-pyusb has been removed from binaries.. needed for lglaf/SALT
pacman -Q python2-pyusb-git || su -c - $LOGINUSR "trizen -S --noconfirm python2-pyusb-git"

# ensure we can talk with dbus here
echo -e "\n... exporting dbus session"
export $(dbus-launch)

# compile dconf db's
echo -e "\n... compiling mAid dconf db"
dbus-launch dconf compile /etc/dconf/db/maid /etc/dconf/db/database.d

# set aliases
echo -e '\n# FWUL aliases\nalias fastboot="sudo fastboot"\n' >> /home/$LOGINUSR/.bashrc

# install additional firmware
pacman -Q bcm4350-firmware || su -c - $LOGINUSR "trizen -S --noconfirm bcm4350-firmware"

# fix blur cache for lockscreen which uses a fix md5-like hash which I cannot figure out
# where it is set
[ -d /var/cache/image-blur/ ] && rm -rf /var/cache/image-blur/
mkdir /var/cache/image-blur/
/usr/lib/deepin-api/image-blur-helper /home/$LOGINUSR/.fwul/fwul_login.png  /var/cache/image-blur/
mv /var/cache/image-blur/*.png /var/cache/image-blur/a64a5cec728db85c5af2fee265b31e5d.png





# ensure proper perms (have to be done BEFORE using any su LOGINUSER cmd which writes to home dir!)
chown -R ${LOGINUSR}.users /home/$LOGINUSR/
###############################################################################################################
#
# UPSTREAM FIX REQUIRED - WORKAROUND SECTION
#
###############################################################################################################

######################
# vbox utils bug
# https://code.binbash.rocks:8443/FWUL/build_fwul/issues/4
RMN=1
pacman -Q virtualbox-guest-utils || RMN=0
[ $RMN == "1" ] && pacman --noconfirm -Rn virtualbox-guest-utils
RMN=1
pacman -Q linux419-virtualbox-guest-modules || RMN=0
[ $RMN == "1" ] && pacman --noconfirm -Rn linux419-virtualbox-guest-modules

# the following generates the vbox-utils.tar and must be done within a running FWUL box
# ! should NOT be part of a build process as makepkg WILL fail !

#pacman -Q lib32-gcc-libs || sudo pacman -S --noconfirm lib32-gcc-libs
#pacman -Q linux419-headers || sudo pacman -S --noconfirm linux419-headers
#rm -rf vbox 
#git clone --depth 1 https://git.archlinux.org/svntogit/community.git -b packages/virtualbox vbox
#cd vbox
#wget https://bugs.archlinux.org/task/61307?getfile=17559 -O vbpatch
#patch -p1 < vbpatch
#cd trunk
#makepkg -Cfsi --noconfirm --needed
#tar cvf vbox-utils.tar virtualbox-guest-utils-*.pkg.tar.* virtualbox-guest-dkms-*.pkg.tar.*
#curl --upload-file vbox-utils.tar https://transfer.sh/

# regular workaround install for vbox-utils.tar when generated by the above once
VBINST=1
pacman -Q virtualbox-guest-utils || VBINST=0
if [ $VBINST == "0" ];then
    #wget -O /tmp/vbox-utils.tar "http://leech.binbash.rocks:8008/FWUL/.repo/vbox-utils.tar"
    cd /tmp && tar xvf /.repo/vbox-utils.tar
    pacman --noconfirm -U /tmp/virtualbox-guest-*
    rm /tmp/virtualbox-guest-*
fi

# ensure we do not remove the module when removing kernel headers
find /usr/src -name dkms.conf -exec mv -v {} {}.disabled \;

systemctl enable vboxservice
# vbox utils bug - end
########################

###############################################################################################################
#
# cleanup
#
###############################################################################################################
echo -e "\nCleanup - locale & MAN pages:"
trizen -Q localepurge || su -c - $LOGINUSR "trizen -S --noconfirm localepurge"
# set the locales we want to keep:
cut -d ' ' -f1 /etc/locale.gen >> /etc/locale.nopurge
cut -d ' ' -f1 /etc/locale.gen | cut -d '.' -f 1 >> /etc/locale.nopurge
cut -d ' ' -f1 /etc/locale.gen | cut -d '.' -f 1 | cut -d "_" -f1 >> /etc/locale.nopurge

# copy minimal needed noto fonts
if [ ! -d /usr/share/fonts/noto-minimal ];then
    mkdir /usr/share/fonts/noto-minimal
    cp /usr/share/fonts/noto/NotoSansMono-* /usr/share/fonts/noto-minimal/
    cp /usr/share/fonts/noto/NotoSans-* /usr/share/fonts/noto-minimal/
fi

# cleanup deepin fallback theme nobody wants to have
[ -d /boot/grub/themes/deepin-fallback ] && rm -rf /boot/grub/themes/deepin-fallback/

# purge locales and manpages:
localepurge -v
for localeinuse in $(find /usr/share/locale/ -maxdepth 1 -type d |cut -d "/" -f5 );do 
    grep -q $localeinuse /etc/locale.gen || rm -rfv /usr/share/locale/$localeinuse
done

echo -e "\nCleanup - pacman:"
IGNPKG="adwaita-icon-theme lvm2 man-db man-pages mdadm nano netctl openresolv pcmciautils reiserfsprogs s-nail vi xfsprogs zsh memtest86+ caribou gnome-backgrounds gnome-themes-extra gnome-themes-standard nemo telepathy-glib zeitgeist gnome-icon-theme progsreiserfs linux316 linux316-virtualbox-guest-modules qt5-tools btrfs-progs ruby rubygems geoip-database-extra deepin-wallpapers linux419-headers acpica bin86 cdrtools dev86 glu gsoap java-environment-common jdk7-openjdk jre7-openjdk jre7-openjdk-headless lib32-glibc libidl2 libstdc++5 libvncserver sdl sdl_ttf vde2 xalan-c xerces-c xorg-server-devel xorg-util-macros lib32-gcc-libs noto-fonts archlinux-appstream-data groff virtualbox-guest-dkms flex xfce4-panel garcon"
for igpkg in $IGNPKG;do
    PFOUND=1
    pacman -Q $igpkg || PFOUND=0 2>&1 >> /dev/null
    [ $PFOUND -eq 1 ] && echo "trying to remove $igpkg as it seems to be installed.." && pacman --noconfirm -Rns -dd $igpkg
    [ $PFOUND -eq 0 ] && echo "PACMAN: package $igpkg not found for removal"
    true
done

echo -e "\nCleanup - python stuff:"
for pydir in "/usr/lib/python2.*" "/usr/lib/python3.*";do
    echo "deleting test dir for $pydir"
    rm -rvf $pydir/test/*
    echo "deleting pyo's,pyc's & pycaches in $pydir"
    find "$pydir" -type f -name "*.py[co]" -delete -print || echo "nothing to clean in $pydir"
    find "$pydir" -type d -name "__pycache__" -delete -print 2>/dev/null || echo "nothing to clean in $pydir"
done

echo -e "\nCleanup - folders:"
DELFOLD="/usr/share/icons/HighContrast /usr/share/icons/hicolor /usr/share/icons/locolor /share/icons/Adwaita /usr/share/icons/gnome /usr/share/icons/Numix-Light /usr/share/icons/gnome \
/usr/share/icons/ePapirus /usr/share/icons/Sea /usr/share/icons/Papirus-Dark /usr/share/icons/Papirus-Light /usr/share/icons/deepin-dark /home/$LOGINUSR/programs/lglafsploit/.git/objects"
for delf in $DELFOLD;do
    [ -d "$delf" ] && rm -vrf "$delf"
    true
done

echo -e "\nCleanup - pacman orphans:"
PMERR=$(pacman --noconfirm -Rns $(pacman -Qtdq) || echo no pacman orphans)
echo -e "\nCleanup - trizen orphans:"
YERR=$(su -c - $LOGINUSR "trizen -Qtd --noconfirm" || echo no trizen orphans)

echo -e "\nCleanup - manpages:"
rm -rvf /usr/share/man/*

echo -e "\nCleanup - docs:"
rm -rvf /usr/share/doc/* /usr/share/gtk-doc/html/*

echo -e "\nCleanup - misc:"
rm -rvf /*.tgz /*.tar.gz /trizen/ /package-query/ /home/$LOGINUSR/.fwul/tmp/* /usr/src/*

echo -e "\nCleanup - archiso:"
rm -rvf /etc/fwul

echo -e "\nCleanup - linux firmware"
rm -rvf /usr/lib/firmware/netronome /usr/lib/firmware/liquidio

# persistent perms for fwul
cat > $RSUDOERS <<EOSUDOERS
%wheel     ALL=(ALL) ALL

# special rules for session & language
%wheel     ALL=(ALL) NOPASSWD: /bin/mount -o remount\,size=* /run/archiso/cowspace
%wheel     ALL=(ALL) NOPASSWD: /bin/umount -l /tmp
%wheel     ALL=(ALL) NOPASSWD: /bin/mv /var/tmp/* /tmp/
%wheel     ALL=(ALL) NOPASSWD: /bin/mv /tmp/locale.conf /etc/locale.conf
%wheel     ALL=(ALL) NOPASSWD: /bin/mv /tmp/environment /etc/environment
# needed for language workaround
%wheel     ALL=(ALL) NOPASSWD: /usr/bin/systemctl restart lightdm

# let the user sync the databases without asking for pw
%wheel     ALL=(ALL) NOPASSWD: /usr/bin/trizen --noconfirm -Sy
%wheel     ALL=(ALL) NOPASSWD: /usr/bin/pacman --noconfirm -Sy
%wheel     ALL=(ALL) NOPASSWD: /usr/bin/pacman -Sy --noconfirm
%wheel     ALL=(ALL) NOPASSWD: /usr/bin/pacman -Sy

# let the user install deps and compiled packages by trizen without asking for pw
%wheel     ALL=(ALL) NOPASSWD: /usr/bin/pacman *.pkg.tar.* -Ud *
%wheel     ALL=(ALL) NOPASSWD: /bin/cp -vf /tmp/trizen-$LOGINUSR/*/*.pkg.tar.* /var/cache/pacman/*

# when using fwul package installer no pw 
%wheel     ALL=(ALL) NOPASSWD: /home/$LOGINUSR/.fwul/install_package.sh *

# special rules for TeamViewer
%wheel     ALL=(ALL) NOPASSWD: /bin/systemctl start teamviewerd
%wheel     ALL=(ALL) NOPASSWD: /bin/systemctl enable teamviewerd

# allow ANY pacman installation without prompting
%wheel     ALL=(ALL) NOPASSWD: /usr/bin/pacman * -U *
%wheel     ALL=(ALL) NOPASSWD: /usr/bin/pacman -U *
%wheel     ALL=(ALL) NOPASSWD: /usr/bin/pacman * -S *
%wheel     ALL=(ALL) NOPASSWD: /usr/bin/pacman -S *

# special rule for Sony Flashtool
%wheel     ALL=(ALL) NOPASSWD: /usr/bin/trizen --noconfirm -S xperia-flashtool
%wheel     ALL=(ALL) NOPASSWD: /bin/cp x10flasher.jar /usr/lib/xperia-flashtool/

# special rule for SP Flashtool
%wheel     ALL=(ALL) NOPASSWD: /usr/bin/trizen --noconfirm -S spflashtool-bin

# special rule for fastboot
%wheel     ALL=(ALL) NOPASSWD: /usr/bin/fastboot *

# SALT
%wheel     ALL=(ALL) NOPASSWD: /home/$LOGINUSR/programs/SALT/salt

# FWUL mode
%wheel     ALL=(ALL) NOPASSWD: /bin/mv /tmp/maid-release /etc/maid-release

# qphotorec
%wheel     ALL=(ALL) NOPASSWD: /usr/bin/qphotorec *

# mAid
%wheel     ALL=(ALL) NOPASSWD: /home/$LOGINUSR/.fwul/install-disk.sh
EOSUDOERS

# set root password
passwd root <<EOSETPWROOTPW
$RPW
$RPW
EOSETPWROOTPW

# set real release info
echo "maidversion=$iso_version" > /etc/maid-release
echo "maidbuild=$(date +%s)" >> /etc/maid-release
if [ "$iso_label" == "mAid_nightly" ];then
    echo "maidtype=nightly" >> /etc/maid-release
else
    echo "maidtype=stable" >> /etc/maid-release
fi
echo "patchlevel=0" >> /etc/maid-release
ln -sf /etc/maid-release /etc/fwul-release

# media fix
[ ! -d /media ] && mkdir /media
chmod 755 /media

# etc fix
chown -R root.root /etc

# set default user for lightdm
test -d /var/lib/lightdm/.cache/lightdm-gtk-greeter || mkdir -p /var/lib/lightdm/.cache/lightdm-gtk-greeter/
echo -e "[greeter]\nlast-user=$LOGINUSR" > /var/lib/lightdm/.cache/lightdm-gtk-greeter/state && chmod 755 /var/lib/lightdm/.cache/lightdm-gtk-greeter/state



# ensure hosts does not contain build stuff
mv /etc/hosts.orig /etc/hosts
########################################################################################
# TEST AREA - TEST AREA - TEST AREA 

echo -e "\nTESTING FWUL BUILD!"

if [ -e /boot/vmlinuz-linux ];then 
    echo "Kernel in place!"
else
    echo "ERROR: Kernel symlink not valid!"
    ls -la /boot
    false
fi

# arch independent requirements
REQFILES="/home/$LOGINUSR/.fwul/wallpaper_fwul.png 
$RSUDOERS
/home/$LOGINUSR/Desktop/LG/open-lglafshell.desktop
/home/$LOGINUSR/Desktop/LG/open-lglafng.desktop
/home/$LOGINUSR/programs/SALT/salt
/home/$LOGINUSR/programs/lglafng/partitions.py
/home/$LOGINUSR/programs/lglaf/partitions.py
/home/$LOGINUSR/programs/kdztools/unkdz.py
/home/$LOGINUSR/Desktop/LG/SALT.desktop
/home/$LOGINUSR/Desktop/ADB.desktop
/home/$LOGINUSR/programs/sadb/starter.sh
/home/$LOGINUSR/programs/sadb/S-ADB.jar
/home/$LOGINUSR/Desktop/Samsung/heimdall.desktop
/home/$LOGINUSR/Desktop/install-TV.desktop
/usr/bin/adb
/usr/bin/fastboot
/usr/bin/heimdall
/usr/bin/trizen
/usr/bin/qphotorec
/home/$LOGINUSR/Desktop/qphotorec.desktop
/home/$LOGINUSR/.fwul/odin-logo.jpg
/home/$LOGINUSR/.fwul/install_spflash.sh
/home/$LOGINUSR/.fwul/install_sonyflash.sh
/home/$LOGINUSR/Desktop/install-sonyflash.desktop
/home/$LOGINUSR/.android/adb_usb.ini
/etc/udev/rules.d/51-android.rules
/home/$LOGINUSR/programs/welcome/welcome.sh
/home/$LOGINUSR/programs/welcome/icons/welcome.png
/home/$LOGINUSR/.config/autostart/welcome.desktop
/home/$LOGINUSR/.config/autostart/z_setlockbg.desktop
/etc/systemd/scripts/fwul-session.sh
/etc/profile.d/fwul-session.sh
/etc/systemd/system/init-mirror.service
/etc/systemd/scripts/init-fwul
/home/$LOGINUSR/Desktop/welcome.desktop
/etc/maid-release
/usr/local/bin/livepatcher.sh
/usr/local/bin/liveupdater.sh
/var/lib/fwul/generic.vars
/var/lib/fwul/generic.func
/home/$LOGINUSR/.fwul/sshkeygen.sh
/home/$LOGINUSR/.config/autostart/sshkeygen.desktop
$CURJAVA
/home/$LOGINUSR/.fwul/pkexecgui
/home/$LOGINUSR/Desktop/tmate.desktop
/home/$LOGINUSR/.fwul/tmate.sh
/home/$LOGINUSR/.fwul/tmate-logo.png
/home/$LOGINUSR/.config/autostart/hex.desktop
/home/$LOGINUSR/.fwul/hex.sh
/home/$LOGINUSR/Desktop/io.github.Hexchat.desktop
/home/$LOGINUSR/Desktop/Xiaomi/miflash.desktop
/home/$LOGINUSR/programs/MiFlash/README.md
/home/$LOGINUSR/programs/MiFlash/Xiaomi.png
/home/$LOGINUSR/programs/MiFlash/starter.sh
/home/$LOGINUSR/programs/ROME/rome
/home/$LOGINUSR/Desktop/rome.desktop
/home/$LOGINUSR/.config/autostart/language.desktop
/home/$LOGINUSR/.fwul/checklang.sh
/etc/lightdm/lightdm.conf
/etc/dconf/profile/user
/etc/dconf/db/database.d/maid
/etc/pacman.ignore
/etc/dconf/db/maid
/home/$LOGINUSR/Desktop/effects.desktop
/home/$LOGINUSR/.fwul/set_de_effects.sh
/.repo/chromium_maid-${buildpkgver}.tar
/home/$LOGINUSR/.fwul/ff.tar
/.repo/spflashtool-bin_maid-${buildpkgver}.tar
/.repo/xperia-flashtool_maid-${buildpkgver}.tar
/.repo/teamviewer_maid-${buildpkgver}.tar
/home/$LOGINUSR/.fwul/unpack_ff.sh
/home/$LOGINUSR/.local/share/applications/ff.desktop
/home/$LOGINUSR/.fwul/install-disk.sh
/home/$LOGINUSR/Desktop/install-maid.desktop
/etc/mAid/maid.vars"

# 32bit requirements (extend with 32bit ONLY.
# If the test is the same for both arch use REQFILES instead)
REQFILES_i686="$REQFILES"

# 64bit requirements (extend with 64bit ONLY. 
# If the test is the same for both arch use REQFILES instead)
REQFILES_x86_64="$REQFILES
/home/$LOGINUSR/Desktop/install-spflash.desktop
/home/$LOGINUSR/Desktop/Samsung/JOdin.desktop
/home/$LOGINUSR/programs/JOdin/starter.sh
/home/$LOGINUSR/programs/JOdin/JOdin3CASUAL"


CURREQ="REQFILES_${arch}"

for req in $(echo -e "${!CURREQ}"|tr "\n" " ");do
    if [ -f "$req" ];then
        echo -e "\t... testing ($arch): $req --> OK"
    else
        echo -e "\t******************************************************************************"
        echo -e "\tERROR: testing ($arch) $req --> FAILED!!"
        echo -e "\t******************************************************************************"
        exit 3
    fi
done

# add a warning when debugging is enabled
if [ "$DEBUG" -eq 1 ];then
        echo -e "\t******************************************************************************"
        echo -e "\tWARNING: DEBUG MODE ENABLED !!!"
        echo -e "\t******************************************************************************"
fi

# TEST AREA - END
########################################################################################

# list the biggest packages installed
pacman --noconfirm -S expac
expac -H M -s "%-30n %m" | sort -rhk 2 | head -n 40
pacman --noconfirm -Rns expac

# create a XDA copy template for the important FWUL package versions
echo -ne '[*]Versions of the main mAid components:\n[INDENT]Kernel -> [B]'version:$(pacman -Qi linux419 |grep Version| cut -d ":" -f 2)'[/B]\nADB and fastboot: '
pacman -Q android-tools | sed 's/ / -> [B]version: /g;s/$/[\/B]/g'
echo -e 'simple-adb GUI -> [B]version: XXXXXXXXXXXXX[/B]'
echo -e 'SALT -> [B]version: '$(egrep -o 'VDIG=.*' /home/$LOGINUSR/programs/SALT/salt.vars | cut -d '"' -f2)'[/B]'
echo -e 'ROME -> [B]version: '$(egrep -o 'VDIG=.*' /home/$LOGINUSR/programs/ROME/rome.vars | cut -d '"' -f2)'[/B]'
echo -e "Firefox (bundled) -> [B]version: $FFVERS[/B]"
echo -e "TeamViewer (installer pre-package) -> [B]version: $TVVERS[/B]"
echo -e "SonyFlash tools (installer pre-packaged) -> [B]version: $SONYVERS[/B]"
echo -e "SPFlash tools (installer pre-packaged) -> [B]version: $SPFTVERS[/B]"
echo -e "Chromium (installer pre-packaged) -> [B]version: $CHRVERS[/B]"
CHLOG="bootimgtool-git heimdall-git deepin-wm lightdm xorg-server virtualbox-guest-utils hexchat testdisk-wip tmate"
for i in $CHLOG;do
        pacman -Q $i | sed 's/ / -> [B]version: /g;s/$/[\/B]/g'
done
echo -e '[/INDENT]'

# fix perms
# https://code.binbash.it:8443/FWUL/build_fwul/issues/58
chown -v root.root /usr
chmod -v 755 /usr
chown -v root.root /usr/share
chmod -v 755 /usr/share

#########################################################################################
# this has always to be the very last thing!
rm -vf $TMPSUDOERS

#!/bin/bash
################################################################################################
# simple wrapper to install latest compatible TV version
################################################################################################
source /etc/maid-release
source /etc/mAid/maid.vars

PKEXEC_USR=$(id -un $PKEXEC_UID)

pacman -Q teamviewer || sudo /home/$PKEXEC_USR/.fwul/install_package.sh pre-pkg teamviewer

pacman -Q teamviewer && systemctl start teamviewerd \
  && cp /usr/share/applications/com.teamviewer.TeamViewer.desktop /home/$PKEXEC_USR/Desktop/ \
  && chmod +x /home/$PKEXEC_USR/Desktop/com.teamviewer.TeamViewer.desktop \
  && chown $PKEXEC_USR /home/$PKEXEC_USR/Desktop/com.teamviewer.TeamViewer.desktop \
  && rm /home/$PKEXEC_USR/Desktop/install-TV.desktop \
  && systemctl enable teamviewerd


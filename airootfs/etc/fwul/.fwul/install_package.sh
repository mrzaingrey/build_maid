#!/bin/bash
###############################################################################################################
#
# Generic grahical installer which can be triggered by CLI
#
###############################################################################################################

source /etc/maid-release
source /etc/mAid/maid.vars

YBIN=/usr/bin/yad

PMAN="$1"               # installer (e.g pacman or trizen)
PKG="$2"                # package name to install
AURHELPER=trizen        # the AUR helper (must have pacman compatible usage syntax)

LOGDIR=/var/log/maid
LOG=${LOGDIR}/install-${PKG}.log
YAD="$YBIN --center --top --title=mAid-installer --window-icon=anaconda"
LOCKFILE=$LOGDIR/pkg.lock

# check/restart as root
ME=$(id -u)

if [ $ME -ne 0 ];then
    echo "restarting with root perms"
    pkexec $0 $@
    exit
fi

# check if we are alone
if [ -f "$LOCKFILE" ];then
    $YAD --image=dialog-warning --width 400 --fixed --height 180 --button=Force:9 --button=Close:0 --text "\nHold your horses!\n\nThere is already an installation in progress.\n\nTry again in a min or so..\n"
    [ $? -eq 0 ] && exit
    $YAD --image=dialog-warning --width 500 --fixed  --button=Yes:1 --button=Abort:0 --text "\nThis can result in unexpected behavior and/or a broken installation!\n\nAre you sure??"
    [ $? -ne 1 ] && exit
    rm $LOCKFILE
fi

[ -z "$PMAN" ]&& echo "missing package manager variable! ABORT" && read -p "PRESS ANY KEY TO QUIT" DUMMY && exit
[ -z "$PKG" ]&& echo "missing package name to install! ABORT" && read -p "PRESS ANY KEY TO QUIT" DUMMY && exit
[ ! -f "$YBIN" ]&& echo "missing yad dependency! ABORT" && read -p "PRESS ANY KEY TO QUIT" DUMMY && exit

[ ! -d "$LOGDIR" ] && mkdir -p $LOGDIR
sudo chmod -R 666 $LOGDIR
echo "starting new log at $(date)" > $LOG
touch $LOCKFILE

# depending on the package manager we need to start it as root or not
case $PMAN in 
    pacman|pre-pkg) PMANEXEC="pacman";;
    *) PMANEXEC="$AURHELPER" ;;
esac

F_EXIT(){
    ECD=$1
    rm $LOCKFILE
    exit $ECD
}

# check for space requirements and warn!
F_CHKSPACE(){
    TARGET="$1"
    for dep in $($PMANEXEC --print-format %s -S $TARGET);do
        TARGETSIZE=$((TARGETSIZE + $dep))
    done

    TARGETSPACE=$((TARGETSIZE/1024/1024))
    AVAILSPACEK=$(df -k --output=avail / |tr -d " " |egrep "[0-9]")
    AVAILSPACE=$((AVAILSPACEK/1024))

    # some packages in AUR do not report their installed size (shame on them!)
    # this has to be checked and warned
    if [ $TARGETSPACE -eq 0 ];then
        $YAD --image=dialog-warning --width 800 --fixed --height 180 --button=Abort:99 --button="Continue installation":0 --form --text "\n\tOops cannot determine installation size (some packages simply does not report them before installing)!\n\n\tYour available disk space: <b>$AVAILSPACE MB</b>!\n\tIf you encounter problems it is maybe related!"
        [ $? -eq 99 ] && echo 99 && F_EXIT
    fi

    # compare it
    if [ "$TARGETSPACE" -ge "$AVAILSPACE" ];then
        echo "$TARGETSPACE"
    else
        echo 0
    fi
}

$YAD --image=anaconda --text-align=center --width 380 --height 50 --form --text "Continuing will start the installation of:\n\n<span color=\"green\">$PKG</span>\n<i>(this requires a working Internet connection)</i>\n\n"

# do the magic
if [ $? -eq 0 ];then
    [ -f "/tmp/install-$PKG-success" ] && rm -f /tmp/install-$PKG-success
    # start a progress bar to give some user feedback
    $PMANEXEC --noconfirm -Sy 2>&1 | tee -a $LOG | sed 's/^/# /g' | $YAD --width=300 --progress --percentage=10 --auto-close --pulsate --enable-log --progress-text="refreshing database..."
    # install the package
    PRECHK=0
    [ "$PMANEXEC" == "pacman" ] && PRECHK=$(F_CHKSPACE "$PKG")
    if [ "$PRECHK" -eq 0 ];then
        if [ $PMAN == "pre-pkg" ];then
            if [ $maidtype == nightly ];then
                TARF="${PKG}_maid-nightly.tar"
            else
                TARF="${PKG}_maid-v${maidversion}.tar"
            fi
            [ -d /tmp/fwulinst ] && rm -rf /tmp/fwulinst
            mkdir /tmp/fwulinst
            cd /tmp/fwulinst
            wget $REPOURL/$TARF -O $TARF -q --show-progress --progress=dot:force 2>&1 |stdbuf -i0 -o0 -e0 egrep -o "[0-9]+%" | $YAD --width=600 --progress --progress-text="downloading: $PKG ..." --auto-close --auto-kill --button=Abort:9
            (pv -n $TARF | tar x) 2>&1 | $YAD --width=600 --progress --progress-text="extracting: $PKG ..." --auto-close --auto-kill --button=Abort:9
            $PMANEXEC --noconfirm -U *.pkg.tar.* 2>&1 | tee -a $LOG | stdbuf -i0 -o0 -e0 sed 's/^/# /g'| $YAD --width=600 --height=400 --progress --enable-log="installing: $PKG ..." --auto-close --log-expanded --auto-kill --button=Abort:9 --pulsate
            [ ${PIPESTATUS[0]} -eq 0 ] && touch /tmp/install-$PKG-success
            cd /tmp && rm -rf fwulinst
        else
            $PMANEXEC -S --noconfirm $PKG 2>&1 | tee -a $LOG | sed 's/^/# /g'| $YAD --width=600 --height=400 --progress --enable-log="installing: $PKG ..." --auto-close --log-expanded --auto-kill --button=Abort:9 --pulsate
	    [ ${PIPESTATUS[0]} -eq 0 ] && touch /tmp/install-$PKG-success
        fi
    else
        [ "$PRECHK" -eq 99 ] && F_EXIT 3
        $YAD --image=error --width 800 --fixed --height 180 --button=Abort --form --text "\n\tERROR not enough disk space!\n\n\tInstalling $PKG requires at least: <b>$PRECHK MB</b>!\n\tYou either need more RAM or use an USB device and boot mAid in persistent mode!"
        F_EXIT 9
    fi

    # check if it was a success or not
    if [ -f "/tmp/install-$PKG-success" ];then
        $YAD --image=checkmark --width 300 --height 100 --form --text "\n\t$PKG installed successfully." --button=Close
    else
       	$YAD --image=script-error --width 600 --height 600 --text "\n\tERROR occured while installing:\n\t$PKG\n" --text-info --filename="$LOG" --button=Close
    fi
fi
F_EXIT

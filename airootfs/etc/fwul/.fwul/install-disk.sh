#!/bin/bash
###############################################################################################################
#
# Install mAid to your disk
#
###############################################################################################################

source /etc/maid-release
source /etc/mAid/maid.vars

DEBUG=0
YBIN=/usr/bin/yad
PMANEXEC=pacman
LOGDIR=/var/log/maid
LOG=${LOGDIR}/install-maid.log
YICON=/home/android/.fwul/mAid_installer_icon.png
YAD="$YBIN --center --top --title=mAid_Installer --window-icon=$YICON"
PKGS="calamares rsync mkinitcpio squashfs-tools gnu-free-fonts os-prober"

[ ! -f "$YBIN" ]&& echo "missing yad dependency! ABORT" && read -p "PRESS ANY KEY TO QUIT" DUMMY && exit

[ ! -d "$LOGDIR" ] && mkdir -p $LOGDIR
sudo chmod -R 666 $LOGDIR
echo "starting new log at $(date)" > $LOG

F_EXIT(){
    ECD=$1
    exit $ECD
}

$YAD --image=$YICON --text-align=center --width 600 --height 200 --form --text "Continuing will flash Manjaro (mAid edition) to your local disk or USB stick!\n(this requires a working Internet connection)\n\n"

# do the magic
if [ $? -eq 0 ];then
    # umounting sharedfolders if any is needed to allow copying $HOME later
    umount -a -t vboxsf 2>&1 | tee -a $LOG | stdbuf -i0 -o0 -e0 sed 's/^/# /g' | $YAD --width=300 --height=400 --progress --percentage=10 --auto-close --pulsate --enable-log --enable-log="umnounting shared folders ..."

    [ -d /tmp/maid ] && rm -rf /tmp/maid
    [ -d /etc/calamares ] && rm -rf /etc/calamares
    git clone $GITURL/maid_installer.git /tmp/maid >> $LOG 2>&1 \
        && cd /tmp/maid \
        && git checkout $maidtype >> $LOG 2>&1 \
        && mv -v /tmp/maid/calamares /etc/ >> $LOG 2>&1
    if [ $? -ne 0 ];then
        echo "error checking out installer sources!" >> $LOG
        $YAD --image=script-error --width 600 --height 800 --text "\n\tERROR occured while preparing the installer!\n\tCannot continue..\n" --text-info --filename="$LOG" --button=Close
        F_EXIT 3
    fi

    $PMANEXEC -Qq calamares || sudo /home/$SUDO_USER/.fwul/install_package.sh pre-pkg maidsetup
    
    # re-check pre-requirements and installer pkg - this is just meant as a FALLBACK as all these come with the bundled installer already!
    for PKG in $PKGS;do
        $PMANEXEC -Qq $PKG
        if [ $? -ne 0 ];then
            $PMANEXEC -S --needed --noconfirm $PKG 2>&1 | tee -a $LOG | stdbuf -i0 -o0 -e0 sed 's/^/# /g'| $YAD --width=600 --height=400 --progress --enable-log="installing: $PKG ..." --auto-close --log-expanded --auto-kill --button=Abort:9 --pulsate
            if [ ${PIPESTATUS[0]} -ne 0 ];then 
                $YAD --image=script-error --width 600 --height 800 --text "\n\tERROR occured while installing:\n\t$PKG\n" --text-info --filename="$LOG" --button=Close
                F_EXIT 3
            fi
        fi
    done
fi
[ $DEBUG == 1 ] && calamares -d >> $LOG 2>&1
[ $DEBUG == 0 ] && calamares >> $LOG 2>&1

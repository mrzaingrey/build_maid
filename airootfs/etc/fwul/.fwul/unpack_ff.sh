#!/bin/bash
################################################################################################
# simple wrapper to unpack and/or run firefox
################################################################################################

pacman -Q firefox
if [ $? -ne 0 ];then 
    sudo pacman -Sy
    mkdir /tmp/fwulinst
    cd /tmp/fwulinst
    (pv -n ~/.fwul/ff.tar | tar x) | yad --title Unpacking_FF --width=600 --progress --progress-text="extracting firefox ..." --auto-close --auto-kill --button=Abort:9
    sudo pacman -U --noconfirm * 2>&1 | stdbuf -i0 -o0 -e0 sed 's/^/# /g' | yad --title Unpacking_FF --width=600 --height=400 --progress --enable-log="installing firefox ..." --auto-close --log-expanded --auto-kill --button=Abort:9 --pulsate
    cd ~ 
    rm -rf /tmp/fwulinst
fi
/usr/lib/firefox/firefox >> /dev/null 2>&1
